<?php

/**
 * Define custom extra fields for Node Title and More Link
 *
 * Implements hook_field_extra_fields().
 */
function view_mode_theme_tools_field_extra_fields() {
  $bundles = field_info_bundles('node');

  foreach ($bundles as $bundle => $array) {
    $extra['node'][$bundle]['display'] = array(
      'field_extra_fields_title' => array(
        'label'       => t('Node Title'),
        'description' => t('A fieldable Node Title supplied by view_mode_theme_tools'),
        'weight'      => 50, // default weight, can be changed on display form by site-builder.
      ),
      'field_extra_fields_more' => array(
        'label'       => t('More Link'),
        'description' => t('A fieldable More Link supplied by view_mode_theme_tools'),
        'weight'      => 50, // default weight, can be changed on display form by site-builder.
      ),
    );

    // disable these fields by default in the display
    view_mode_theme_tools_hide_new_extra_field('node', $bundle, 'field_extra_fields_title');
    view_mode_theme_tools_hide_new_extra_field('node', $bundle, 'field_extra_fields_more');
  }

  return $extra;
}


/**
 * Make the extra fields available to the render array of the display
 *
 * Implements hook_node_view().
 */
function view_mode_theme_tools_node_view($node, $view_mode, $langcode) {
  // Gather Extra fields
  $extra_fields = field_info_extra_fields('node', $node->type, 'display');

  // Node Title
  if(!empty($extra_fields['field_extra_fields_title']) && $extra_fields['field_extra_fields_title']['display'][$view_mode]['visible']) {
    $link = $extra_fields['field_extra_fields_title']['display'][$view_mode]['settings']['link'];
    $selector = $extra_fields['field_extra_fields_title']['display'][$view_mode]['settings']['selector'];

    // render title as a link
    if (isset($link) && $link == 'yes') {
      $node->nodetitle = l($node->title, 'node/' . $node->nid);
    } else {
      $node->nodetitle = $node->title;
    }

    // render a tag selector wrapper
    if (isset($selector)) {
      $node->nodetitle = '<' . $selector . '>' . $node->nodetitle . '</' . $selector .'>';
    }

    // create a renderable array
    $node->content['field_extra_fields_title'] = array(
      '#prefix' => '<div class="field field_extra_fields_title">',
      '#markup' => $node->nodetitle,
      '#suffix' => '</div>'
    );
  }

  // More Link
  if(!empty($extra_fields['field_extra_fields_more']) && $extra_fields['field_extra_fields_more']['display'][$view_mode]['visible']) {
    $label = $extra_fields['field_extra_fields_more']['display'][$view_mode]['settings']['label'];
    $class = $extra_fields['field_extra_fields_more']['display'][$view_mode]['settings']['class'];
    $class = explode(' ', $class);

    // create a renderable array
    $node->content['field_extra_fields_more'] = array(
      '#prefix' => '<div class="field field_extra_fields_more">',
      '#markup' => l($label, 'node/' . $node->nid, array('attributes' => array('class' => $class))),
      '#suffix' => '</div>'
    );
  }
}


/**
 * Allow settings for extra fields to be administrated from the display form
 *
 * Implements hook_form_FORM_ID_alter
 * to alter the content-type display form
 */
function view_mode_theme_tools_form_alter(&$form, &$form_state) {
  // Gather Extra fields
  $extra = view_mode_theme_tools_field_extra_fields();

  // If the fields are not supported for the entity type and bundle
  // we're loading the form then returning early.
  if (!isset($form['#entity_type']) || !isset($extra[$form['#entity_type']], $extra[$form['#entity_type']][$form['#bundle']]) || !isset($form['#extra'])) {
    return;
  }

  // Filter extra fields by those the field ui module is choosing to show in the form.
  $extra_fields = array_keys($extra[$form['#entity_type']][$form['#bundle']]['display']);
  $extra_fields = array_intersect($extra_fields, $form['#extra']);

  if (empty($extra_fields)) {
    return;
  }

  // Get current settings for this entity/bundle
  // (@see form_submit : I store additional settings
  // into this variable to avoid create another variable
  // in variable table)
  $settings = field_bundle_settings($form['#entity_type'], $form['#bundle']);

  // Retrieve current view-mode
  $view_mode = $form['#view_mode'];

  foreach ($extra_fields as $field_name) {

    // Get my own settings from "settings" key
    $extra_fields_settings = isset($settings['extra_fields']['display'][$field_name],
      $settings['extra_fields']['display'][$field_name][$view_mode],
      $settings['extra_fields']['display'][$field_name][$view_mode]['settings'])
      ? $settings['extra_fields']['display'][$field_name][$view_mode]['settings']
      : array();

    // Get current format (hidden or visible, to
    // show settings form when needed only)
    $format_type = isset($form_state['values'],
      $form_state['values']['fields'],
      $form_state['values']['fields'][$field_name],
      $form_state['values']['fields'][$field_name]['type'])
      ? $form_state['values']['fields'][$field_name]['type']
      : $form['fields'][$field_name]['format']['type']['#default_value'];

    // Build the settings form. Here, the "settings_edit"
    // is the crucial key to use. You must start with
    // it and build other settings fields into.
    if ($format_type == 'visible' && $field_name == 'field_extra_fields_title') {
      $form['fields'][$field_name]['settings_summary'] = array(
        '#type' => 'container',
        '#title' => t('Node Title Settings'),

        'link' => array(
          '#type' => 'select',
          '#title' => t('Link to content'),
          '#options' => array(
            'no'    => t('No'),
            'yes' => t('yes'),
          ),
          '#default_value' => isset($extra_fields_settings['link']) ? $extra_fields_settings['link'] : 'no',
        ),
        'selector' => array(
          '#type' => 'textfield',
          '#title' => t('Selector'),
          '#default_value' => isset($extra_fields_settings['selector']) ? $extra_fields_settings['selector'] : 'h2',
        ),
      );
    }
    if ($format_type == 'visible' && $field_name == 'field_extra_fields_more') {
      $form['fields'][$field_name]['settings_summary'] = array(
        '#type' => 'container',
        '#title' => t('More Link Settings'),

        'label' => array(
          '#type' => 'textfield',
          '#title' => t('Link label'),
          '#default_value' => isset($extra_fields_settings['label']) ? $extra_fields_settings['label'] : 'Read More',
        ),
        'class' => array(
          '#type' => 'textfield',
          '#title' => t('Link class'),
          '#default_value' => isset($extra_fields_settings['class']) ? $extra_fields_settings['class'] : 'more-link',
        ),
      );
    }
  }
  // Don't forget to add a submit handler if you need to process/save
  // configuration for your fields.
  $form['#submit'][] = 'view_mode_theme_tools_extrafields_form_submit';
}


/**
 * Save the settings for extra fields
 *
 * Custom submit callback to store
 * settings
 */
function view_mode_theme_tools_extrafields_form_submit($form, &$form_state) {

  // Get current module extra fields
  $extra = view_mode_theme_tools_field_extra_fields();
  // Filter extra fields by those the field ui module is chosing to show in the form.
  $extra_fields = array_keys($extra[$form['#entity_type']][$form['#bundle']]['display']);
  $extra_fields = array_intersect($extra_fields, $form['#extra']);

  // Get current settings and view-mode
  $settings = field_bundle_settings($form['#entity_type'], $form['#bundle']);
  $view_mode = $form['#view_mode'];

  // Iterate on each extra fields and get
  // current form values
  foreach ($extra_fields as $field_name) {

    $extra_fields_settings = isset($form_state['values'],
      $form_state['values']['fields'],
      $form_state['values']['fields'][$field_name],
      $form_state['values']['fields'][$field_name]['settings_summary'])
      ? $form_state['values']['fields'][$field_name]['settings_summary']
      : array();

    // Add current form values into the "settings" key
    // of the current managed field
    $settings['extra_fields']['display'][$field_name][$view_mode]['settings'] = $extra_fields_settings;
  }

  // Finally, recall field_bundle_settings
  // with settings as last param to save it
  // into the database.
  $settings = field_bundle_settings($form['#entity_type'], $form['#bundle'], $settings);
}


/**
 * Utility function to hide any newly created extra_fields.
 *
 * @param $entity_type string
 *   Entity type on which the new extra field will reside.
 *
 * @param $bundle string
 *   Bundle on which the new extra field will reside.
 *
 * @param $field_name string
 *   Machine name of extra field.
 */
function view_mode_theme_tools_hide_new_extra_field($entity_type, $bundle, $field_name) {
  $settings = field_bundle_settings($entity_type, $bundle);
  $settings_updated = FALSE;
  $entity_info = entity_get_info($entity_type, $bundle);
  $view_modes = array_keys($entity_info['view modes']);
  if (!in_array('default', $view_modes)) {
    $view_modes[] = 'default';
  }
  foreach ($view_modes as $view_mode) {
    if (!isset($settings['extra_fields']['display'][$field_name][$view_mode])) {
      $settings_updated = TRUE;
      $settings['extra_fields']['display'][$field_name][$view_mode] = array(
        'weight'  => 100,
        'visible' => FALSE,
      );
    }
  }
  if ($settings_updated) {
    field_bundle_settings($entity_type, $bundle, $settings);
  }
}