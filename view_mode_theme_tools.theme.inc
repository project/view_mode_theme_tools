<?php

/**
 * Add additional settings on the view mode display pages in the backend
 *
 * Implements hook_form_FORM_ID_alter().
 * Using hook_form_field_ui_display_overview_form_alter.
 */
function view_mode_theme_tools_form_field_ui_display_overview_form_alter(&$form, &$form_state) {
  // Check if this is a node display and not the default display
  if($form['#entity_type'] == 'node' && $form['#view_mode'] != 'default') {
    // Get current settings for this entity/bundle
    // (@see form_submit : I store additional settings
    // into this variable to avoid create another variable
    // in variable table)
    $settings = field_bundle_settings($form['#entity_type'], $form['#bundle']);

    // Retrieve current view-mode
    $view_mode = $form['#view_mode'];

    // Get our own settings from "settings" key
    $vmtt_settings = isset($settings['vmtt'][$view_mode]) ? $settings['vmtt'][$view_mode] : array();


    // We are going to add a fieldset with a form inside to enable a block for each node display
    // Add additional settings vertical tab.
    if (!isset($form['additional_settings'])) {
      $form['additional_settings'] = array(
        '#type' => 'vertical_tabs',
        '#theme_wrappers' => array('vertical_tabs'),
        '#prefix' => '<div>',
        '#suffix' => '</div>',
        '#tree' => TRUE,
      );
      $form['#attached']['js'][] = 'misc/form.js';
      $form['#attached']['js'][] = 'misc/collapse.js';
    }

    // Add View Mode Theme Tools vertical tab settings.
    $form['additional_settings']['view_mode_theme_tools'] = array(
      '#type' => 'fieldset',
      '#title' => t('View Mode Theme Tools'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#parents' => array('additional_settings'),
    );

    // Add template suggestion field
    $form['additional_settings']['view_mode_theme_tools']['vmtt_template_suggestion'] = array(
      '#title' => t('Custom template suggestion for this display'),
      '#description' => t('Your suggestion will be prepended with \'node--\' for discovery purposes. (Example: your--custom-template)'),
      '#type' => 'textfield',
      '#default_value' => isset($vmtt_settings['vmtt_template_suggestion']) ? $vmtt_settings['vmtt_template_suggestion'] : '',
    );

    // Add custom class field
    $form['additional_settings']['view_mode_theme_tools']['vmtt_classes'] = array(
      '#title' => t('Custom classes for this display'),
      '#description' => t('Separate multiple classes with a space. (Example: your-custom-class1 your-custom-class2)'),
      '#type' => 'textfield',
      '#default_value' => isset($vmtt_settings['vmtt_classes']) ? $vmtt_settings['vmtt_classes'] : '',
    );

    $form['additional_settings']['view_mode_theme_tools']['token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => array('node'), // The token types that have specific context. Can be multiple token types like 'term' and/or 'user'
      '#global_types' => FALSE, // A boolean TRUE or FALSE whether to include 'global' context tokens like [current-user:*] or [site:*]. Defaults to TRUE.
      '#click_insert' => FALSE, // A boolean whether to include the 'Click this token to insert in into the the focused textfield' JavaScript functionality. Defaults to TRUE.
    );

    // submit values to block submission function
    $form['#submit'][] = 'view_mode_theme_tools_theme_form_submit';
  }
}

/**
 * Save the settings for extra fields
 *
 * Custom submit callback to store
 * settings
 */
function view_mode_theme_tools_theme_form_submit($form, &$form_state) {
  // Get current settings and view-mode
  $settings = field_bundle_settings($form['#entity_type'], $form['#bundle']);
  $view_mode = $form['#view_mode'];

  $vmtt_template_suggestion_settings = isset($form_state['values'],
    $form_state['values']['additional_settings'],
    $form_state['values']['additional_settings']['vmtt_template_suggestion'])
    ? $form_state['values']['additional_settings']['vmtt_template_suggestion']
    : '';

  // Add current form values into the "settings" key
  $settings['vmtt'][$view_mode]['vmtt_template_suggestion'] = $vmtt_template_suggestion_settings;

  $vmtt_classes_settings = isset($form_state['values'],
    $form_state['values']['additional_settings'],
    $form_state['values']['additional_settings']['vmtt_classes'])
    ? $form_state['values']['additional_settings']['vmtt_classes']
    : '';

  // Add current form values into the "settings" key
  $settings['vmtt'][$view_mode]['vmtt_classes'] = $vmtt_classes_settings;


  // Finally, recall field_bundle_settings
  // with settings as last param to save it
  // into the database.
  $settings = field_bundle_settings($form['#entity_type'], $form['#bundle'], $settings);
}